package br.edu.unisep.helpdesk.domain.validator.helpdesk;

import br.edu.unisep.helpdesk.domain.dto.helpdesk.RegisterHelpDeskDto;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.*;
@Component
public class RegisterHelpDeskValidator {

    public void validate(RegisterHelpDeskDto registerHelpDesk){
        Validate.notBlank(registerHelpDesk.getNameIssuer(), MESSAGE_REQUIRED_HELPDESK_NAMEISSUER);
        Validate.notBlank(registerHelpDesk.getEmailIssuer(), MESSAGE_REQUIRED_HELPDESK_EMAILISSUER);
        Validate.notBlank(registerHelpDesk.getNameResponder(), MESSAGE_REQUIRED_HELPDESK_NAMEREPONDER);
        Validate.notBlank(registerHelpDesk.getEmailResponder(), MESSAGE_REQUIRED_HELPDESK_EMAILRESPONDER);
        Validate.notBlank(registerHelpDesk.getTitle(), MESSAGE_REQUIRED_HELPDESK_TITLE);
        Validate.notBlank(registerHelpDesk.getDescription(), MESSAGE_REQUIRED_HELPDESK_DESCRIPTION);
        Validate.notNull(registerHelpDesk.getOpenDate(), MESSAGE_REQUIRED_HELPDESK_OPDENDATE);
        Validate.notNull(registerHelpDesk.getStatus(), MESSAGE_REQUIRED_HELPDESK_STATUS);
    }
}
