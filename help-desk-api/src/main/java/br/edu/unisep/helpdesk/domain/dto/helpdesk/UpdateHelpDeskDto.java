package br.edu.unisep.helpdesk.domain.dto.helpdesk;

import lombok.Data;

@Data
public class UpdateHelpDeskDto extends RegisterHelpDeskDto{

    private Integer ticket_id;
}
