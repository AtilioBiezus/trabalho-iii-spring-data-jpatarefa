package br.edu.unisep.helpdesk.controller.helpdesk;

import br.edu.unisep.helpdesk.domain.dto.helpdesk.HelpDeskDto;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.UpdateHelpDeskDto;
import br.edu.unisep.helpdesk.domain.usecase.helpdesk.*;
import br.edu.unisep.helpdesk.response.DeafultResponse;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@RequestMapping("/helpdesk")
public class HelpDeskController {

    private final FinAllHelpDesksUseCase findAllHelpDesks;
    private final FindHelpDeskByNameIssuer findByNameIssuer;
    private final FindHelpDeskByNameResponder findByNameResponder;
    private final RegisterHelpDeskUseCase registerHelpDesk;
    private final UpdateHelpDeskUseCase updateHelpDesk;


    @GetMapping
    public ResponseEntity<DeafultResponse<List<HelpDeskDto>>> findAll() {
        var helpdesks = findAllHelpDesks.execute();

        return helpdesks.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DeafultResponse.of(helpdesks));
    }

    @GetMapping("/byNameIssuer")
    public ResponseEntity<DeafultResponse<List<HelpDeskDto>>> findByNameIssuer(@RequestParam("nameIssuer") String nameIssuer) {
        var helpdesks = findByNameIssuer.execute(nameIssuer);

        return helpdesks == null ?
                ResponseEntity.notFound().build() :
                ResponseEntity.ok(DeafultResponse.of(helpdesks));
    }

    @GetMapping("/byNameResponder")
    public ResponseEntity<DeafultResponse<List<HelpDeskDto>>> findByTitle(@RequestParam("nameResponder") String nameResponder) {
        var helpdesk = findByNameResponder.execute(nameResponder);

        return helpdesk.isEmpty() ?
                ResponseEntity.noContent().build() :
                ResponseEntity.ok(DeafultResponse.of(helpdesk));
    }

    @PostMapping
    public ResponseEntity<DeafultResponse<Boolean>> save(@RequestBody RegisterHelpDeskDto helpdesk) {
        registerHelpDesk.execute(helpdesk);
        return ResponseEntity.ok(DeafultResponse.of(true));
    }

    @PutMapping
    public ResponseEntity<DeafultResponse<Boolean>> update(@RequestBody UpdateHelpDeskDto helpDesk){
        System.out.println("Inicio");
        updateHelpDesk.execute(helpDesk);
        return ResponseEntity.ok(DeafultResponse.of(true));
    }
}