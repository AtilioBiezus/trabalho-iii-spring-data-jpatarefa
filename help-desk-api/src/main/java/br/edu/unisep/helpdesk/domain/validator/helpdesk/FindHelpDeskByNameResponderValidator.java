package br.edu.unisep.helpdesk.domain.validator.helpdesk;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.MESSAGE_REQUIRED_HELPDESK_NAMEREPONDER;
@Component
public class FindHelpDeskByNameResponderValidator {

    public void validate(String nameResponder){

        Validate.isTrue(nameResponder.length() > 0, MESSAGE_REQUIRED_HELPDESK_NAMEREPONDER);
    }
}
