package br.edu.unisep.helpdesk.domain.validator.helpdesk;


import br.edu.unisep.helpdesk.domain.dto.helpdesk.UpdateHelpDeskDto;
import lombok.AllArgsConstructor;
import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.MESSAGE_REQUIRED_HELPDESK_TICKET_ID;

@Component
@AllArgsConstructor
public class UpdateHelpDeskValidator {

    public void validate(UpdateHelpDeskDto helpdesk){
        Validate.notNull(helpdesk.getTicket_id(), MESSAGE_REQUIRED_HELPDESK_TICKET_ID);
        Validate.isTrue(helpdesk.getTicket_id() > 0, MESSAGE_REQUIRED_HELPDESK_TICKET_ID);
    }
}
