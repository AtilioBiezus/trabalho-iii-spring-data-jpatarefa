package br.edu.unisep.helpdesk.data.dao.helpdesk;

import br.edu.unisep.helpdesk.data.entity.helpdesk.HelpDesk;
import org.springframework.stereotype.Repository;
import br.edu.unisep.helpdesk.data.hibernate.HibernateSessionFactory;

import java.util.Date;
import java.util.List;

@Repository
public class HelpDeskDao {

    public List<HelpDesk> findAll() {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from HelpDesk order by ticket_id", HelpDesk.class);
        var result = query.list();

        session.close();

        return result;
    }

    public List<HelpDesk> findByNameIssuer (String nameIssuer) {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from HelpDesk where nameIssuer = :pNameIssuer", HelpDesk.class);
        query.setParameter("pNameIssuer", nameIssuer);
        var result = query.list();

        session.close();

        return result;
    }

    public List<HelpDesk> findByNameResponder(String nameResponder) {
        var session = HibernateSessionFactory.getSession();

        var query = session.createQuery("from HelpDesk where nameResponder = :pNameResponder", HelpDesk.class);
        query.setParameter("pNameResponder", nameResponder);
        var result = query.list();

        session.close();

        return result;
    }

    public void save(HelpDesk helpdesk) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            session.save(helpdesk);
            transaction.commit();
        } catch (Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }

        session.close();
    }

    public void update(HelpDesk helpDesk) {
        var session = HibernateSessionFactory.getSession();
        var transaction = session.beginTransaction();

        try {
            if (helpDesk.getStatus() == 2) {
                helpDesk.setCloseDate(new Date());
            }
            session.update(helpDesk);
            transaction.commit();
        } catch (Exception error) {
            error.printStackTrace();
            transaction.rollback();
        }
        session.close();
    }
}

