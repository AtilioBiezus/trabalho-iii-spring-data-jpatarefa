package br.edu.unisep.helpdesk.domain.dto.helpdesk;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RegisterHelpDeskDto {

    private String nameIssuer;

    private String emailIssuer;

    private String nameResponder;

    private String emailResponder;

    private String title;

    private String description;

    private Date openDate;

    private Integer status;

    private Date closeDate;
}
