package br.edu.unisep.helpdesk.domain.validator;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ValidationMessages {

    public static final String MESSAGE_REQUIRED_HELPDESK_TICKET_ID = "Informe o id!";
    public static final String MESSAGE_REQUIRED_HELPDESK_NAMEISSUER = "Informe o nome do registrante!";
    public static final String MESSAGE_REQUIRED_HELPDESK_EMAILISSUER = "Informe o email do registrante!";
    public static final String MESSAGE_REQUIRED_HELPDESK_NAMEREPONDER = "Informe o nome do responsável";
    public static final String MESSAGE_REQUIRED_HELPDESK_EMAILRESPONDER = "Informe o email do responsavel";
    public static final String MESSAGE_REQUIRED_HELPDESK_TITLE = "Informe o titulo!";
    public static final String MESSAGE_REQUIRED_HELPDESK_DESCRIPTION = "Informe a descrição!";
    public static final String MESSAGE_REQUIRED_HELPDESK_OPDENDATE = "Informe a data de abertura!";
    public static final String MESSAGE_REQUIRED_HELPDESK_STATUS = "Informe o status!";
}