package br.edu.unisep.helpdesk.domain.builder.helpdesk;

import br.edu.unisep.helpdesk.data.entity.helpdesk.HelpDesk;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.HelpDeskDto;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.UpdateHelpDeskDto;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HelpDeskBuilder {

    public List<HelpDeskDto> from(List<HelpDesk> helpDesks){
        return helpDesks.stream().map(this::from).collect(Collectors.toList());
    }

    public HelpDeskDto from(HelpDesk helpDesk) {
        return new HelpDeskDto(
                helpDesk.getTicket_id(),
                helpDesk.getNameIssuer(),
                helpDesk.getEmailIssuer(),
                helpDesk.getNameResponder(),
                helpDesk.getEmailResponder(),
                helpDesk.getTitle(),
                helpDesk.getDescription(),
                helpDesk.getOpenDate(),
                helpDesk.getStatus(),
                helpDesk.getCloseDate()
        );
    }

    public HelpDesk from(UpdateHelpDeskDto updateHelpDesk){

        HelpDesk helpDesk = from((RegisterHelpDeskDto) updateHelpDesk);
        helpDesk.setTicket_id(updateHelpDesk.getTicket_id());

        return helpDesk;
    }

    public HelpDesk from(RegisterHelpDeskDto registerHelpDesk){
        HelpDesk helpDesk = new HelpDesk();
        helpDesk.setNameIssuer(registerHelpDesk.getNameIssuer());
        helpDesk.setEmailIssuer(registerHelpDesk.getEmailIssuer());
        helpDesk.setNameResponder(registerHelpDesk.getNameResponder());
        helpDesk.setEmailResponder(registerHelpDesk.getNameResponder());
        helpDesk.setTitle(registerHelpDesk.getTitle());
        helpDesk.setDescription(registerHelpDesk.getDescription());
        helpDesk.setOpenDate(registerHelpDesk.getOpenDate());
        helpDesk.setStatus(registerHelpDesk.getStatus());
        helpDesk.setCloseDate(registerHelpDesk.getCloseDate());

        return helpDesk;
    }
}