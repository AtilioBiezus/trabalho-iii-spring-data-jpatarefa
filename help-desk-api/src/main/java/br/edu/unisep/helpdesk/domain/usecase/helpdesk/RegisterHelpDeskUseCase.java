package br.edu.unisep.helpdesk.domain.usecase.helpdesk;

import br.edu.unisep.helpdesk.data.dao.helpdesk.HelpDeskDao;
import br.edu.unisep.helpdesk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.RegisterHelpDeskDto;
import br.edu.unisep.helpdesk.domain.validator.helpdesk.RegisterHelpDeskValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class RegisterHelpDeskUseCase {

    private final RegisterHelpDeskValidator validator;
    private final HelpDeskBuilder builder;
    private final HelpDeskDao helpDeskDao;

    public void execute(RegisterHelpDeskDto registerHelpDesk){
        validator.validate(registerHelpDesk);

        var helpdesk = builder.from(registerHelpDesk);
        helpDeskDao.save(helpdesk);
    }
}
