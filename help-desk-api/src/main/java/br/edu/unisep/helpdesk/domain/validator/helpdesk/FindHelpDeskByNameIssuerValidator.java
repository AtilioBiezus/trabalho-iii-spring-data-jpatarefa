package br.edu.unisep.helpdesk.domain.validator.helpdesk;

import org.apache.commons.lang3.Validate;
import org.springframework.stereotype.Component;

import static br.edu.unisep.helpdesk.domain.validator.ValidationMessages.MESSAGE_REQUIRED_HELPDESK_NAMEISSUER;
@Component
public class FindHelpDeskByNameIssuerValidator {

    public void validate(String nameIssuer){

        Validate.isTrue(nameIssuer.length() > 0, MESSAGE_REQUIRED_HELPDESK_NAMEISSUER);
    }
}
