package br.edu.unisep.helpdesk.domain.dto.helpdesk;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class HelpDeskDto {

    private Integer ticket_id;

    private String nameIssuer;

    private String emailIssuer;

    private String nameResponder;

    private String emailResponder;

    private String title;

    private String description;

    private Date openDate;

    private Integer status;

    private Date closeDate;

}
