package br.edu.unisep.helpdesk.data.entity.helpdesk;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "tickets")
public class HelpDesk {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ticket_id")
    private Integer ticket_id;

    @Column(name = "name_issuer")
    private String nameIssuer;

    @Column(name = "email_issuer")
    private String emailIssuer;

    @Column(name = "name_responder")
    private String nameResponder;

    @Column(name = "email_responder")
    private String emailResponder;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "open_date")
    private Date openDate;

    @Column(name = "status")
    private Integer status;

    @Column(name = "close_date")
    private Date closeDate;
}
