package br.edu.unisep.helpdesk.domain.usecase.helpdesk;


import br.edu.unisep.helpdesk.data.dao.helpdesk.HelpDeskDao;
import br.edu.unisep.helpdesk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.UpdateHelpDeskDto;
import br.edu.unisep.helpdesk.domain.validator.helpdesk.UpdateHelpDeskValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class UpdateHelpDeskUseCase {

    private final UpdateHelpDeskValidator validator;
    private final HelpDeskBuilder builder;
    private final HelpDeskDao helpDeskDao;

    public void execute(UpdateHelpDeskDto updateHelpDesk){
        validator.validate(updateHelpDesk);

        var helpdesk = builder.from(updateHelpDesk);
        helpDeskDao.update(helpdesk);
    }
}
