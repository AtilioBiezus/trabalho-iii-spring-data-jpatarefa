package br.edu.unisep.helpdesk.domain.usecase.helpdesk;


import br.edu.unisep.helpdesk.data.dao.helpdesk.HelpDeskDao;
import br.edu.unisep.helpdesk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.HelpDeskDto;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
@AllArgsConstructor
public class FinAllHelpDesksUseCase {

    private final HelpDeskDao helpDeskDao;
    private final HelpDeskBuilder builder;

    public List<HelpDeskDto> execute(){
        var helpdesks = helpDeskDao.findAll();
        return builder.from(helpdesks);
    }
}
