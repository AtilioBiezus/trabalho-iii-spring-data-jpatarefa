package br.edu.unisep.helpdesk;

import br.edu.unisep.helpdesk.config.CorsConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HelpDeskApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelpDeskApiApplication.class, args);
	}

}
