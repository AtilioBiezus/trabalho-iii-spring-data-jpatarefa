package br.edu.unisep.helpdesk.response;

import lombok.Getter;

import java.util.List;

@Getter
public class DeafultResponse<T> {

    private final String message;
    private final T result;

    private DeafultResponse(String message, T result){
        this.message = message;
        this.result = result;
    }

    private DeafultResponse(T result){
        this("", result);
    }

    public static <R> DeafultResponse<R> of(R result){
        return new DeafultResponse<>(result);
    }

    public static <R> DeafultResponse<List<R>> of(List<R> result){
        return new DeafultResponse<>(result);
    }

    public static <R> DeafultResponse<R> of(String message, R result){
        return new DeafultResponse<>(message, result);
    }
}
