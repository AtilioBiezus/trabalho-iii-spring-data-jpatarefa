package br.edu.unisep.helpdesk.domain.usecase.helpdesk;

import br.edu.unisep.helpdesk.data.dao.helpdesk.HelpDeskDao;
import br.edu.unisep.helpdesk.domain.builder.helpdesk.HelpDeskBuilder;
import br.edu.unisep.helpdesk.domain.dto.helpdesk.HelpDeskDto;
import br.edu.unisep.helpdesk.domain.validator.helpdesk.FindHelpDeskByNameIssuerValidator;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class FindHelpDeskByNameIssuer {

    private final FindHelpDeskByNameIssuerValidator validator;
    private final HelpDeskDao helpDeskDao;
    private final HelpDeskBuilder builder;

    public List<HelpDeskDto> execute(String nameIssuer){
        validator.validate(nameIssuer);

        var helpdesks = helpDeskDao.findByNameIssuer(nameIssuer);
        return builder.from(helpdesks);
    }
}
