import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { HelpDesk, RegisterHelpDesk } from "../model/helpdesk.model";
import { DefaultResponse } from "../model/response.model";

const URL_BASE = "http://localhost:8080/helpdesk/"
const URL_FIND_BY_NAME_ISSUER = URL_BASE + "byNameIssuer?nameIssuer="
const URL_FIND_BY_NAME_RESPONDER = URL_BASE + "byNameResponder?nameResponder="


@Injectable({
    providedIn: 'root'
})
export class HelpDeskService {

    constructor(private http: HttpClient) {

    }

    findAll(): Observable<DefaultResponse<HelpDesk[]>> {
        return this.http.get<DefaultResponse<HelpDesk[]>>(URL_BASE)
    }

    findByNameIssuer(nameIssuer: string): Observable<DefaultResponse<HelpDesk[]>> {
        return this.http.get<DefaultResponse<HelpDesk[]>>(URL_FIND_BY_NAME_ISSUER + nameIssuer)
    }

    findByNameResponder(nameResponder: string): Observable<DefaultResponse<HelpDesk[]>> {
        return this.http.get<DefaultResponse<HelpDesk[]>>(URL_FIND_BY_NAME_RESPONDER + nameResponder)       
    }

    save(helpdesk: RegisterHelpDesk): Observable<DefaultResponse<boolean>> {
        return this.http.post<DefaultResponse<boolean>>(URL_BASE, helpdesk, {observe: "body"})
    }

    update(helpdesk: HelpDesk): Observable<DefaultResponse<boolean>> {
        return this.http.put<DefaultResponse<boolean>>(URL_BASE, helpdesk, {observe: "body"})
    }

}