import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { ReactiveFormsModule } from "@angular/forms";
import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './ui/home/app.component';
import { ListHelpDeskComponent } from './ui/list-helpdesk/list-helpdesk.component';
import { RegisterHelpDeskComponent } from './ui/register-helpdesk/register-helpdesk.component';
import { DetailsHelpDeskComponent } from './ui/details-helpdesk/details-helpdesk.component';

@NgModule({
  declarations: [
    AppComponent,
    ListHelpDeskComponent,
    RegisterHelpDeskComponent,
    DetailsHelpDeskComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
