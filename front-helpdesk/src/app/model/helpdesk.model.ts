export class HelpDesk {
    public ticket_id: number
    public nameIssuer: string
    public emailIssuer: string
    public nameResponder: string
    public emailResponder: string
    public title: string
    public description: string
    public openDate: Date
    public status: number
    public closeDate: Date
}

export class RegisterHelpDesk {
    public nameIssuer: string
    public emailIssuer: string
    public nameResponder: string
    public emailResponder: string
    public title: string
    public description: string

    constructor(nameIssuer: string,
                emailIssuer: string,
                nameResponder: string,
                emailResponder: string,
                title: string,
                description: string
                ) {
        
        this.nameIssuer = nameIssuer
        this.emailIssuer = emailIssuer
        this.nameResponder = nameResponder
        this.emailResponder = emailResponder
        this.title = title
        this.description = description
    }
}