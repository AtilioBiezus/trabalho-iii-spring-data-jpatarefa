import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DetailsHelpDeskComponent } from './ui/details-helpdesk/details-helpdesk.component';
import { ListHelpDeskComponent } from './ui/list-helpdesk/list-helpdesk.component';
import { RegisterHelpDeskComponent } from './ui/register-helpdesk/register-helpdesk.component';

const routes: Routes = [
    { path: 'list-helpdesk', component: ListHelpDeskComponent },
    { path: 'register-helpdesk', component: RegisterHelpDeskComponent },
    { path: 'details-helpdesk/:helpdeskId', component: DetailsHelpDeskComponent },
    { path: '', pathMatch: 'full', redirectTo: '/list-helpdesk'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }