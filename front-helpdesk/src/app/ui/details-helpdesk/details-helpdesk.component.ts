import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HelpDesk } from 'src/app/model/helpdesk.model';


@Component({
    selector: 'app-details-helpdesk',
    templateUrl: './details-helpdesk.component.html',
    styleUrls: ['./details-helpdesk.component.css']
})
export class DetailsHelpDeskComponent implements OnInit {

    helpdesk : HelpDesk = new HelpDesk()

    constructor(private router: Router) {
        this.helpdesk = this.router.getCurrentNavigation().extras.state.helpdesk
     }

    ngOnInit(): void { }


    list(){
        this.router.navigateByUrl('list-helpdesk')
    }
}
