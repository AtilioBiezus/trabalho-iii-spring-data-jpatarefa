import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegisterHelpDesk } from 'src/app/model/helpdesk.model';
import { HelpDeskService } from 'src/app/service/helpdesk.service';

@Component({
    selector: 'app-register-helpdesk',
    templateUrl: './register-helpdesk.component.html',
    styleUrls: ['./register-helpdesk.component.css']
})
export class RegisterHelpDeskComponent {

    formHelpDesk = this.formBuilder.group({
        nameIssuer: ['', [ Validators.required ] ],
        emailIssuer: ['', [ Validators.required ] ],
        nameResponder: ['', [ Validators.required ] ],
        emailResponder: ['', [ Validators.required ] ],
        title: ['', [ Validators.required ] ],
        description: ['', [ Validators.required ] ]
    })

    constructor(private formBuilder: FormBuilder,
                private router: Router,
                private service: HelpDeskService) { }

    save() {
        let helpdesk = new RegisterHelpDesk(
            this.nameIssuer.value,
            this.emailIssuer.value,
            this.nameResponder.value,
            this.emailResponder.value,
            this.title.value,           
            this.description.value
        )      
        
        this.service.save(helpdesk).subscribe(
            result => this.cancel()
        )
    }

    cancel() {
        this.router.navigateByUrl("/")
    }

    get nameIssuer() {
        return this.formHelpDesk.controls.nameIssuer
    }

    get emailIssuer() {
        return this.formHelpDesk.controls.emailIssuer
    }

    get nameResponder() {
        return this.formHelpDesk.controls.nameResponder
    }

    get emailResponder() {
        return this.formHelpDesk.controls.emailResponder
    }

    get description() {
        return this.formHelpDesk.controls.description
    }

    get title() {
        return this.formHelpDesk.controls.title
    }
}
