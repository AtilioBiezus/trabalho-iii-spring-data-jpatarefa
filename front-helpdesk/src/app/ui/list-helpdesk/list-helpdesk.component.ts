import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { HelpDesk } from 'src/app/model/helpdesk.model';
import { DefaultResponse } from 'src/app/model/response.model';
import { HelpDeskService } from 'src/app/service/helpdesk.service';

@Component({
    selector: 'app-list-helpdesk',
    templateUrl: './list-helpdesk.component.html',
    styleUrls: ['./list-helpdesk.component.css'],
})
export class ListHelpDeskComponent implements OnInit {

    helpdesk: HelpDesk[] = []

    formSearch = this.formBuilder.group({
        searchType: [1, [Validators.required]],
        searchText: ['', [Validators.required]]
    })
 
    constructor(private formBuilder: FormBuilder, 
                private helpDeskService: HelpDeskService,
                private http: HttpClient) { }

    ngOnInit() {
        this.findHelpDesk()
    }

    findHelpDesk() {
        let result = this.http.get<DefaultResponse<HelpDesk[]>>("http://localhost:8080/helpdesk")
        result.subscribe(response => {this.helpdesk = response.result})
    }

    search() {
        if (this.searchType.value == 1) {
            this.helpDeskService.findByNameIssuer(this.searchText.value).subscribe(
                response => this.onSearchResult(response.result)
            )
        } else {
            this.helpDeskService.findByNameResponder(this.searchText.value).subscribe(
                response => this.onSearchResult(response.result)
            )
        }
    }

    onSearchResult(result: HelpDesk[]) {
        if (result != null) {
            this.helpdesk = result
        }
    }

    clearSearch() {
        this.searchText.setValue("")
        this.searchType.setValue(1)
        this.findHelpDesk()
    }

    finishProcess(helpdesk: HelpDesk){
        if(helpdesk.status === 1){
            helpdesk.status = 2
            this.helpDeskService.update(helpdesk)
            .subscribe(
                result => {this.findHelpDesk()}
            )
        }
    }

    getStatus(helpdesk: HelpDesk){
        if(helpdesk.status == 1){
            return 'Aberto'
        }
        return 'Fechado'
    }

    dateFormater(formater: Date){
        if(formater){
            return new Date(formater).toLocaleDateString()
        }else{
            return ""
        }
    }

    get searchType() {
        return this.formSearch.controls.searchType
    }

    get searchText() {
        return this.formSearch.controls.searchText
    }
}
